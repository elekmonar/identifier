package network.elekmonar.identifier.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import network.elekmonar.modular.spec.identifier.ClassifierIdentifierManager;

@Stateless
public class ClassifierIdentifierManagerImpl implements ClassifierIdentifierManager, Serializable {

	private static final long serialVersionUID = 4688950088959541402L;
	
	private static final String RESOURCE_ID_BY_LABEL_QUERY = "select id from metamodel.info_resource where label = ?";
	
	private static final String INSTANCE_SET_MAX_ID_QUERY = "select max(id) from classifier.instance_set where (id::bit(40)>>24)::int4 = ?";
	//select substring((id::bit(64)::text), 1, 16)::bit(13) || (id::bit(48)<<8)::bit(40) from classifier.time_zone;
	//select (substring((id::bit(64)::text), 1, 16)::bit(13) || (id::bit(48)<<8)::bit(40))::bit(53)::bigint from classifier.time_zone;
	
	//substring(id::bit(64), 4, 13)::bit(13)::int
	//(id::bit(48)<<8)::bit(40)
	//select (substring(id::bit(64), 4, 13)::bit(13) || (id::bit(48)<<8)::bit(40))::bit(53)::bigint from classifier.time_zone;
	
	@Resource(lookup = "java:jboss/datasources/elekmonarDS")
	private DataSource dataSource;
	
	@Inject
	private CurrentResourceIds currentResourceIds;
	
	@Inject
	private Event<ExceptionToCatchEvent> exceptionToCatch;
	
	@Override
	public Long nextClassifierId(String resourceLabel) {
		try (
				Connection conn = dataSource.getConnection();
		) {
			Short resourceId = getResourceIdByLabel(conn, resourceLabel);
			currentResourceIds.initializeIfNecessary(conn, resourceLabel, resourceId);
			return currentResourceIds.nextId(resourceLabel);
		} catch(SQLException e) {
			exceptionToCatch.fire(new ExceptionToCatchEvent(e));
		}				
		
		return null;
	}
	
	public Long getClassifierId(Short resourceId, Integer originalId) {
		String resIdBinary = Integer.toBinaryString(resourceId);
		String originalIdBinary = Integer.toBinaryString(originalId);
//		String normalizedResourceId = StringUtils.leftPad(resIdBinary, 16, "0");
//		String normalizedOriginalId = StringUtils.leftPad(originalIdBinary, 48, "0");
		String normalizedResourceId = StringUtils.leftPad(resIdBinary, 13, "0");
		String normalizedOriginalId = StringUtils.leftPad(originalIdBinary, 40, "0");		
		String binaryString = normalizedResourceId.concat(normalizedOriginalId);
		return Long.parseLong(binaryString, 2);
	}

	@Override
	public Long nextInstanceSetId(Short resourceId, Integer workspaceId) {
		Long nextId = null;
		try (
				Connection conn = dataSource.getConnection();
		) {
			Long base = (Long.valueOf(workspaceId)<<24)|Long.valueOf(resourceId);
			Long initialId = (Long.valueOf(workspaceId)<<40)|(Long.valueOf(resourceId)<<24);			

			try (
					PreparedStatement pstmt = conn.prepareStatement(INSTANCE_SET_MAX_ID_QUERY);								
			) {
				pstmt.setLong(1, base);
				try (
						ResultSet rs = pstmt.executeQuery();	
				) {
					rs.next();
					Long maxId = rs.getLong(1);
					if (maxId != 0) {
						nextId = maxId + 1;
					} else {
						nextId = initialId + 1;
					}
				}
				
			}
		} catch(SQLException e) {
			exceptionToCatch.fire(new ExceptionToCatchEvent(e));
		}				
		
		System.out.println("next.id = " + nextId);
		
		return nextId;
	}

	private Short getResourceIdByLabel(Connection conn, String label) throws SQLException {		
		try (
				PreparedStatement pstmt = conn.prepareStatement(RESOURCE_ID_BY_LABEL_QUERY);								
		) {
			pstmt.setString(1, label);
			try (
					ResultSet rs = pstmt.executeQuery();	
			) {
				if (rs.next()) {
					return rs.getShort(1);
				}
			}
			
		}		
		
		return null;
	}

	@Override
	public List<Long> getInstanceSetsIds(Integer workspaceId) {
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Short extractResourceId(Long classifierId) {
//		return Short.parseShort(StringUtils.leftPad(Long.toBinaryString(classifierId), 64, "0").substring(0, 16), 2);
		return Short.parseShort(StringUtils.leftPad(Long.toBinaryString(classifierId), 53, "0").substring(0, 13), 2);
	}
	
}