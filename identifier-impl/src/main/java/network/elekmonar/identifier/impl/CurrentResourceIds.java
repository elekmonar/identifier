package network.elekmonar.identifier.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;

@ApplicationScoped
public class CurrentResourceIds implements Serializable {

	private static final long serialVersionUID = -885292804419241570L;
	
	private static final String MAX_ID_QUERY = "select max(id) from classifier.%s";
	
	private Map<String, AtomicLong> currentIds;
		
	@PostConstruct
	private void doInit() {
		currentIds = new ConcurrentHashMap<>();
	}

	public Long nextId(String resourceLabel) {
		return currentIds.get(resourceLabel).incrementAndGet();
	}

	protected void initializeIfNecessary(Connection conn, String resourceLabel, Short resourceId) throws SQLException {
		if (currentIds.containsKey(resourceLabel)) {
			return;
		}
		
		String tableName = resourceLabel.replaceAll("(.)(\\p{Upper})", "$1_$2").toLowerCase();
		Long maxId = null;
		
		try (
				Statement stmt = conn.createStatement();				
				ResultSet rs = stmt.executeQuery(String.format(MAX_ID_QUERY, tableName));
		) {
			rs.next();
			maxId = rs.getLong(1);
			if (maxId == 0) {
				String resIdBinary = Integer.toBinaryString(resourceId);
				String normalizedResourceId = StringUtils.leftPad(resIdBinary, 16, "0");
				String binaryString = normalizedResourceId.concat(StringUtils.repeat('0', 48));
				maxId = Long.parseLong(binaryString, 2);								
			}
		}
		
		currentIds.put(resourceLabel, new AtomicLong(maxId));
	}
	
}